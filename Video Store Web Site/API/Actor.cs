﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Nancy.ModelBinding;


namespace Video_Store_Web_Site.API
{
    public class SearchRequest
    {
        public string StageFirstName { get; set; }
        public string StageLastName { get; set; }
        public string BirthCity { get; set; }
        public string BirthStateOrProvince { get; set; }
    }

    public class SearchResponse : SearchRequest
    {
        public DateTime DateOfBirth;

        public SearchResponse(Model.Actor actor)
        {
            StageFirstName = actor.StageFirstName;
            StageLastName = actor.StageLastName;
            BirthCity = actor.BirthCity;
            BirthStateOrProvince = actor.BirthStateOrProvince;
            DateOfBirth = actor.DateOfBirth;
        }
    }

    public class Actor : BaseModule
    {
        public Actor() : base("actor")
        {
            Post["/search"] = _ =>
            {
                var searchParameters = this.Bind<SearchRequest>();
                var session = _sessionFactory.GetCurrentSession();
               
                QueryBuilder builder = new QueryBuilder("Actor");
                builder.CheckSearchParameter(searchParameters.StageLastName, "StageLastName", DataType.String);
                builder.CheckSearchParameter(searchParameters.StageFirstName, "StageFirstName", DataType.String);
                builder.CheckSearchParameter(searchParameters.BirthCity, "BirthCity", DataType.String);
                builder.CheckSearchParameter(searchParameters.BirthStateOrProvince, "BirthStateOrProvince", DataType.String);
               
                using (var transaction = session.BeginTransaction())
                {
                    var query = builder.ToQuery(session);
                    
                    var response = new List<SearchResponse>();
                   
                    foreach (var actor in query.List())
                    {
                        response.Add(new SearchResponse((Model.Actor) actor));    
                    }                    
                    transaction.Commit();
                    
                    
                    return response;
                }

            };
        }
    }
}