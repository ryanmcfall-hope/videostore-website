﻿using System;
using System.Collections.Generic;
using Mappings;
using Model;
using Nancy;
using NHibernate;
using NHibernate.Mapping;

namespace Video_Store_Web_Site.API
{
    public abstract class BaseModule : NancyModule
    {
        protected ISessionFactory _sessionFactory;
        
        protected static Model.ZipCode Holland = new Model.ZipCode()
        {
            Code = "49423",
            City = "Holland",
            State = "MI"
        };

        protected static Model.ZipCode Zeeland = new Model.ZipCode()
        {
            Code = "49464",
            City = "Zeeland",
            State = "MI"
        };

        protected static Model.Store ChicagoDriveStore = new Store()
        {
            ZipCode = Holland,
            StreetAddress = "1234 Chicago Drive",
            PhoneNumber = "8675309"
        };

        protected static Model.Customer Cusack = new Model.Customer()
        {
            Name = new Name()
            {
                Title = "Dr.",
                First = "Charles",
                Middle = "A.",
                Last = "Cusack"
            },
            EmailAddress = "cusack@hope.edu",
            Phone = "616-395-7271",
            Password = "4erzLe",
            StreetAddress = "27 Graves Place",
            ZipCode = Holland
        };

        protected static Model.Customer McFall = new Model.Customer()
        {
            Name = new Name()
            {
                Title = "Dr.",
                First = "Ryan",
                Middle = "L.",
                Last = "McFall"
            },
            EmailAddress = "mcfall@hope.edu",
            Phone = "616-395-7952",
            Password = "Tenn1s",
            StreetAddress = "1260 Winterwood Ln",
            ZipCode = Zeeland
        };

        protected static Model.Movie Hoosiers = new Model.Movie()
        {
            MovieTitle = "Hoosiers",
            Year = 1986,
            Language = "English",
            Director = "Anspaugh, David",
            RunningTimeInMinutes = 114,
            PrimaryGenre = new Model.Genre() { Name = "Drama"},            
        };

        protected static Model.Video HoosiersVideo = new Video()
        {
            Movie = Hoosiers, NewArrival = true, Store = ChicagoDriveStore
        };

        protected static Model.Movie YouveGotMail = new Model.Movie()
        {
            MovieTitle = "You've Got Mail",
            Year = 1998
        };

        protected static Model.Video YouveGotMailVideo = new Video()
        {
            Movie = YouveGotMail,
            NewArrival = false,
            Store = ChicagoDriveStore
        };

        protected static List<Model.Rental> McFallRentals = new List<Rental>()
        {
             new Model.Rental(McFall, YouveGotMailVideo) {RentalDate = DateTime.Now.AddDays(-2), ReturnDate = DateTime.Now.AddDays(-1)},
             new Model.Rental(McFall, HoosiersVideo)
        };

        protected static List<Model.Rental> CusackRentals = new List<Rental>()
        {
            new Model.Rental(Cusack, HoosiersVideo)
        };

        protected BaseModule(string path) : base("/api/" + path)
        {            
            _sessionFactory = SessionFactory.CreateSessionFactory();   
        }
    }
}