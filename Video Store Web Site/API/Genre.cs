﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Video_Store_Web_Site.API
{
    public class Genre : BaseModule
    {
        public Genre() : base("genre")
        {
            Get["/"] = _ =>
            {
                //  Replace this with code that
                //  1.  Gets the current session
                //  2.  Begins a transaction 
                //  3.  Uses session.CreateQuery to create a query on the Genre object; no criteria are necessary
                //  4.  Uses the .List method on the Query object to get a list of all of the Genres
                //  5.  Commits the transaction
                //  6.  Returns the list of Genre objects that was found
                return new List<Model.Genre>()
                {
                    new Model.Genre() {Name = "Comedy"},
                    new Model.Genre() {Name = "Drama"},
                    new Model.Genre() {Name = "Romance"}
                };
            };
        }
    }
}